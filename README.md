# Taller Grupal 1: Grocery Shopping - Instacard

## Contexto de negocio (Businnes Understanding)

### Antecedentes Instacart
Instacart es una empresa con operación en Estados Unidos, la cual presta sus servicios de entrega en comestibles el mismo día. El acceso se realiza mediante la web y/o aplicaciones App, y los productos son entregados por un comprador. 

A partir de estudio realizado en 2017 por el FMI-The voice of food rentail, se identifica que la generación Millenias tienen una tendencia a utilizar los medios on-line con mas frecuencia de las aplicaciones App y compras on-line y confían menos en la tiendas físicas que las demás generaciones; los productos más probables de adquirir de forma on-line son: comida para mascotas, golosinas, comida de bebes, productos de limpieza y dulces; los productos menos probables de comprar de forma on-line son: Drogas-sin restricción médica, artículos frescos de panadería, carnes y mariscos frescos, productos lácteos refrigerados. Las compras on-line se proyecta según statista.com, un crecimiento exponencial entre el 2012 al 2021, pasando de 2 billones de dólares a 29,7 billones de dólares, por lo que este mercado tiene un atractivo importante y a su ves competitivo para asegurar dos frentes fundamentales, el primero el crecimiento continuo del negocio y el segundo el asegurameinto de la satisfaccion al clientes mas frecuentes impactando la fidelidad.

Se proporciona entre 4 y 100 de sus pedidos, con la secuencia de productos comprados en cada pedido. Adicionalmente proporcionamos la semana y la hora del dia, en que se realiza el pedido, y una medida relativa del tiempo entre los pedidos.

En la publicación de los datos, se presenta brevemente el conjunto de datos y por qué es importante para Instacart proporcionar un enlace para dara conocer los mismos con miras a  que la comunidad de aprendizaje automático use estos datos para diseñar modelos para predecir comportamientos y sugerir elementos para incluir en el carrito de compras.


### Objetivos de negocio

1. Identificar productos o categorías de productos con potencial de comercialización al venderlos en conjunto.

2. Generar recomendaciones y/o estrategias específicas separadas para su tienda virtual y sus tiendas físicas.

3. Idenficar patrones o en encontrar nuevo conocimiento a partir de las bases brindadadas, para comprender las dinamicas del comportamiento de sus usuarios o productos.

### Criterios de éxito de negocio

La cadena está la espera de un reporte utilizando la metodología CRISP-DM que identifique e ilustre patrones de comportamiento en las compras
de supermercado

### Metas de minería de datos

1. Determinar mediante reglas de asociación los productos que tenga un alto potencial de venderse en conjunto y proponer las estrategias adecuadas de combinación en función de los resultados. 

2. Establecer recomendaciones para las tiendas virtuales y físicas a partir de la investigación de tendencias de negocio y la exploración de grupos de preferencias a partir de análisis cluster.  

3. Realizar un análisis de productos y comportamiento de compras con el fin de recomendar estrategías de negocio