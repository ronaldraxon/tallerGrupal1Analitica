---
title: 'modulo reglas asociación: Grocery Shopping - Instacard'
author: "Ronald Rodríguez; Sergio Tiria; Andrés Giraldo"
date: "August 11, 2018"
output:
  word_document: default
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE, echo=FALSE}
knitr::opts_chunk$set(echo = TRUE)
#paquetes<-c("psych","e1071","readxl","R.utils","dplyr","wordcloud","ggpubr")
#install.packages(paquetes)
#install.packages("ggpubr")
library(R.utils)
library(dplyr)
library(wordcloud)
library(ggplot2)
library(ggpubr)
library(plyr)
library(plotly)
library(psych)
library(tm)
library(dendextend)
library(kableExtra)
library(treemap)
library(gridExtra)
library(arules)
library(arulesViz)
library(data.table)
library(Matrix)
#library(readxl)
```

#### Modelo priori - reglas de asociación

De acuerdo con lo identificado en el análisis exploratorio, se identifican los departamentos de mayor relevancia en ventas para análisis de reglas de asociación entre los productos que se encuentran al interior de cada departamento. Inicialmente se generan los archivos de trabajo por cada 1 de los departamentos a analizar (produce, dairyEggs, Snacks, Beverages, personal care, frozen). Cada archivo contiene la información de cada uno de los productos que es pedido asociado a la orden correspondiente.

```{r construc_archivos_departamentos, eval=F, echo=F}

produce <- cleanedProductSet[cleanedProductSet$department=="produce",]
dairyEggs <- cleanedProductSet[cleanedProductSet$department=="dairy eggs",]
snacks <- cleanedProductSet[cleanedProductSet$department=="snacks",]
beverages <- cleanedProductSet[cleanedProductSet$department=="beverages",]
frozen <- cleanedProductSet[cleanedProductSet$department=="frozen",]

write.csv2(produce,file="datasource/produce.csv",col.names=TRUE,sep = ",")
write.csv2(dairyEggs,file="datasource/dairyEggs.csv",col.names=TRUE,sep = ",")
write.csv2(snacks,file="datasource/snacks.csv",col.names=TRUE,sep = ",")
write.csv2(beverages,file="datasource/beverages.csv",col.names=TRUE,sep = ",")
write.csv2(frozen,file="datasource/frozen.csv",col.names=TRUE,sep = ",")

personalCare <- cleanedProductSet[cleanedProductSet$department=="personal care",]
pets <- cleanedProductSet[cleanedProductSet$department=="pets",]
missing <- cleanedProductSet[cleanedProductSet$department=="missing",]
other <- cleanedProductSet[cleanedProductSet$department=="other",]
bulk <- cleanedProductSet[cleanedProductSet$department=="bulk",]

write.csv2(personalCare,file="datasource/personalCare.csv",col.names=TRUE,sep = ",")
write.csv2(pets,file="datasource/pets.csv",col.names=TRUE,sep = ",")
write.csv2(missing,file="datasource/missing.csv",col.names=TRUE,sep = ",")
write.csv2(other,file="datasource/other.csv",col.names=TRUE,sep = ",")
write.csv2(bulk,file="datasource/bulk.csv",col.names=TRUE,sep = ",")


```


##### Modelo priori - reglas de asociación departamento de cuidado personal

A continuación se presenta el desarrollo de modelo y análisis correspondiente para el departamento de productos de cuidado personal o "personal care".  

Dado el gran tamaño de la base de datos, se trabaja con muestras aleatorias. Se genera selección de 30,000 ordenes que tienen productos del departamento personal care. La matriz de transacciones es construida tomando todos los productos que se encuentran presentes en las 30,000 ordenes seleccionadas. En el código siguiente se presenta la selección de muestra y forma de construcción de la matriz de transacciones.


```{r personalcare_matriz_transacciones, eval=T, echo=TRUE}


mpcf<- read.csv(file="datasource/personalCare.csv", header=TRUE, sep=";")
mpcf <- select(mpcf,order_id, user_id,order_number,product_name, product_id)
mpcf <- mpcf%>%
               group_by(order_id, user_id, product_name, product_id)%>%
                summarise(conteo = n())

msorder<-mpcf%>%
               group_by(order_id)%>%
                summarise(conteo = n())

msmuestra <- sample( 1:nrow(msorder), 30000)
msorder <- msorder[ msmuestra, ]
mpc<-merge(x = mpcf, y = msorder, by = "order_id", all = TRUE)
mpc<-mpc[complete.cases(mpc),]

od<-table(mpc$order_id)
od<-as.data.frame(od)
od$order_id<-as.factor(od$Var1)
od$order_rec<-as.factor(1:nrow(od))
pid<-table(mpc$product_id)
pid<-as.data.frame(pid)
pid$product_id<-as.factor(pid$Var1)
pid$product_rec<-as.factor(1:nrow(pid))
mpc<-merge(x = mpc, y = od, by = "order_id", all = TRUE)
mpc<-merge(x = mpc, y = pid, by = "product_id", all = TRUE)
mpc <- select(mpc,order_id, user_id, product_name, product_id, order_rec, product_rec, conteo.x)
mpc<-mpc[complete.cases(mpc),]

mat <- sparseMatrix(
  i = as.integer(mpc$order_rec), 
  j = as.integer(mpc$product_rec), 
  x = mpc$conteo.x,
  dimnames = list(levels(mpc$order_rec), levels(mpc$product_rec))
)

mat <- as.matrix(mat)

colname <- mpc%>%
               group_by(product_name, product_id, product_rec)%>%
                summarise(conteo = n())
colname<-colname[order(colname$product_rec, colname$product_id, colname$product_name),]
colnames(mat) <- colname$product_name
matriz_rulesmpc<-as(mat,"transactions")

```

Del código anterior se obtiene la matriz de transacciones, la cual relaciona para cada orden o transaccion los productos que fueron seleccionados y ordenados por el usuario.

A continuación se aplica el algoritmo a priori para determinar las reglas de asociación presentes.

```{r algoritmo_priori_personalcare, eval=T, echo=FALSE}

rulesmpc<-apriori(matriz_rulesmpc,parameter=list(support=0.00008,confidence=0.5))
```

```{r algoritmo_priori_personalcare, eval=T, echo=TRUE}

itemFrequencyPlot(matriz_rulesmpc, topN=20,  cex.names=.5)
summary(rulesmpc)
inspectDT(rulesmpc)
plot(rulesmpc, measure=c("support","lift"), shading="confidence")
subrules2 <- head(sort(rulesmpc, by="lift"), 10)
plot(subrules2, method="graph")
write(rulesmpc,file="salidas_rules/rules_personal_care.csv",sep=";",dec=",")
```
Al realizar el análisis de reglas de asociación por productos al interior del departamento, es necesario tener en cuenta la dimensión de cantidad de productos existentes, solo en el departamento de cuidado personal se tienen más de 6.500 productos que presentan ventas. Lo anterior tiene implicaciones frente a la elección adecuada de el indicador de Support para análisis de reglas; este indicador representa el porcentaje de veces que un par de productos se encuentran en una misma transacción respecto al total de transacciones, pero en casos como el del presente ejercicio el tener una cantidad tan alta de productos lleva a obtener indicadores support bastante bajos. Sin embargo al revisar frente al indicador de lift se identifican reglas de asociación con un alto lift. 
Se establece un indicador de Support de 0.00008, con el cual se obtienen 57 reglas de asociación, todas con valores de lift bastante altos.  Se identifica como la asociación mas importante la que se da entre productos del cuidado para el cabello, encontrandose una asociación directa entre la compra de productos de Shampoo y acondicionador en una misma transacción, siendo estos los productos centrales para este departamento. Se resaltan multiples asociaciones entre este tipo de productos dada la variedad de marcas y distinciones existentes, pero siempre encontrandose la misma relación.  

Se resaltan igualmente relaciones entre productos de cuidado facial en jabones y tratamientos hidratantes. igualmente se identifican relaciones entre productos de suplementos calmantes o para sueño. Otra relación importante esta marcada por jabones para manos de lavanda y limon, los cuales corresponden a 2 de los principales productos de venta de acuerdo al gráfico del top 20 de productos más vendidos en este departamento. 
En general se puede concluir que en el departamento de cuidado personal las asociaciones se dan entre productos de las misma línea de cuidado, por lo cual una recomendación en la tienda virtual puede ser presentar en forma separada productos como shampoo y acondicionadores, de forma que se lleve al usuario a visualizar otra diversidad de productos antes de llegar a la combinación que en un porcentaje muy alto va a llevar. 


##### Modelo priori - reglas de asociación departamento de bebidas

A continuación se presenta construcción de matriz de transacciones para el departamento de bebidas.

```{r beverages_matriz_transacciones, eval=T, echo=TRUE}

mbf<- read.csv(file="datasource/beverages.csv", header=TRUE, sep=";")

mbf <- select(mbf,order_id, user_id,order_number,product_name, product_id)
mbf <- mbf%>%
               group_by(order_id, user_id, product_name, product_id)%>%
                summarise(conteo = n())
mborder<-mbf%>%
               group_by(order_id)%>%
                summarise(conteo = n())

mbmuestra <- sample( 1:nrow(mborder), 30000)
mborder <- mborder[ mbmuestra, ]
mb<-merge(x = mbf, y = mborder, by = "order_id", all = TRUE)
mb<-mb[complete.cases(mb),]

od<-table(mb$order_id)
od<-as.data.frame(od)
od$order_id<-as.factor(od$Var1)
od$order_rec<-as.factor(1:nrow(od))
pid<-table(mb$product_id)
pid<-as.data.frame(pid)
pid$product_id<-as.factor(pid$Var1)
pid$product_rec<-as.factor(1:nrow(pid))
mb<-merge(x = mb, y = od, by = "order_id", all = TRUE)
mb<-merge(x = mb, y = pid, by = "product_id", all = TRUE)
mb <- select(mb,order_id, user_id, product_name, product_id, order_rec, product_rec, conteo.x)
mb<-mb[complete.cases(mb),]

mat <- sparseMatrix(
  i = as.integer(mb$order_rec), 
  j = as.integer(mb$product_rec), 
  x = mb$conteo.x,
  dimnames = list(levels(mb$order_rec), levels(mb$product_rec))
)
mat <- as.matrix(mat)

colnamemb <- mb%>%
               group_by(product_name, product_id, product_rec)%>%
                summarise(conteo = n())
colnamemb<-colnamemb[order(colnamemb$product_rec, colnamemb$product_id, colnamemb$product_name),]
colnames(mat) <- colnamemb$product_name

matriz_rulesmb<-as(mat,"transactions")

```

A continuación se aplica el algoritmo a priori para determinar las reglas de asociación presentes entre los productos del departamento de bebidas.

```{r algoritmo_priori_beverages, eval=T, echo=FALSE}

rulesmb<-apriori(matriz_rulesmb,parameter=list(support=0.001,confidence=0.5))

```

```{r algoritmo_priori_beverages, eval=T, echo=TRUE}
itemFrequencyPlot(matriz_rulesmb, topN=20,  cex.names=.5)
summary(rulesmb)
inspectDT(rulesmb)
plot(rulesmb, measure=c("support","lift"), shading="confidence")
subrules2 <- head(sort(rulesmb, by="lift"), 10)
plot(subrules2, method="graph")
write(rulesmb,file="salidas_rules/rules_beverages.csv",sep=";",dec=",")
```



##### Modelo priori - reglas de asociación departamento de snacks

A continuación se presenta construcción de matriz de transacciones para el departamento de snacks.

```{r matriztransacciones_snacks, eval=T, echo=TRUE}


msf<- read.csv(file="datasource/snacks.csv", header=TRUE, sep=";")

msf <- select(msf,order_id, user_id,order_number,product_name, product_id)
msf <- msf%>%
               group_by(order_id, user_id, product_name, product_id)%>%
                summarise(conteo = n())

msorder<-msf%>%
               group_by(order_id)%>%
                summarise(conteo = n())

msmuestra <- sample( 1:nrow(msorder), 30000)
msorder <- msorder[ msmuestra, ]
ms<-merge(x = msf, y = msorder, by = "order_id", all = TRUE)
ms<-ms[complete.cases(ms),]

od<-table(ms$order_id)
od<-as.data.frame(od)
od$order_id<-as.factor(od$Var1)
od$order_rec<-as.factor(1:nrow(od))
pid<-table(ms$product_id)
pid<-as.data.frame(pid)
pid$product_id<-as.factor(pid$Var1)
pid$product_rec<-as.factor(1:nrow(pid))
ms<-merge(x = ms, y = od, by = "order_id", all = TRUE)
ms<-merge(x = ms, y = pid, by = "product_id", all = TRUE)
ms <- select(ms,order_id, user_id, product_name, product_id, order_rec, product_rec, conteo.x)
ms<-ms[complete.cases(ms),]

mat <- sparseMatrix(
  i = as.integer(ms$order_rec), 
  j = as.integer(ms$product_rec), 
  x = ms$conteo.x,
  dimnames = list(levels(ms$order_rec), levels(ms$product_rec))
)
mat <- as.matrix(mat)

colnamems <- ms%>%
               group_by(product_name, product_id, product_rec)%>%
                summarise(conteo = n())
colnamems<-colnamems[order(colnamems$product_rec, colnamems$product_id, colnamems$product_name),]
colnames(mat) <- colnamems$product_name

matriz_ruless<-as(mat,"transactions")

```


A continuación se aplica el algoritmo a priori para determinar las reglas de asociación presentes entre los productos del departamento de snacks.

```{r algoritmo_priori_snacks, eval=T, echo=TRUE}
ruless<-apriori(matriz_ruless,parameter=list(support=0.0004,confidence=0.5))
```


```{r algoritmo_priori_snacks, eval=T, echo=TRUE}

itemFrequencyPlot(matriz_ruless, topN=20,  cex.names=.5)
summary(ruless)
inspectDT(ruless)
plot(ruless, measure=c("support","lift"), shading="confidence")
subrules2 <- head(sort(ruless, by="lift"), 10)
plot(subrules2, method="graph")
write(ruless,file="salidas_rules/rules_snacks.csv",sep=";",dec=",")
```


##### Modelo priori - reglas de asociación departamento de frozen

A continuación se presenta construcción de matriz de transacciones para el departamento de congelados o frozen.

```{r frozen_matriz_transacciones, eval=T, echo=TRUE}

mff<- read.csv(file="datasource/frozen.csv", header=TRUE, sep=";")

mff <- select(mff,order_id, user_id,order_number,product_name, product_id)
mff <- mff%>%
               group_by(order_id, user_id, product_name, product_id)%>%
                summarise(conteo = n())
mforder<-mff%>%
               group_by(order_id)%>%
                summarise(conteo = n())

mfmuestra <- sample( 1:nrow(mforder), 30000)
mforder <- mforder[ mfmuestra, ]
mf<-merge(x = mff, y = mforder, by = "order_id", all = TRUE)
mf<-mf[complete.cases(mf),]

od<-table(mf$order_id)
od<-as.data.frame(od)
od$order_id<-as.factor(od$Var1)
od$order_rec<-as.factor(1:nrow(od))
pid<-table(mf$product_id)
pid<-as.data.frame(pid)
pid$product_id<-as.factor(pid$Var1)
pid$product_rec<-as.factor(1:nrow(pid))
mf<-merge(x = mf, y = od, by = "order_id", all = TRUE)
mf<-merge(x = mf, y = pid, by = "product_id", all = TRUE)
mf <- select(mf,order_id, user_id, product_name, product_id, order_rec, product_rec, conteo.x)
mf<-mf[complete.cases(mf),]

mat <- sparseMatrix(
  i = as.integer(mf$order_rec), 
  j = as.integer(mf$product_rec), 
  x = mf$conteo.x,
  dimnames = list(levels(mf$order_rec), levels(mf$product_rec))
)
mat <- as.matrix(mat)

colnamemf <- mf%>%
               group_by(product_name, product_id, product_rec)%>%
                summarise(conteo = n())
colnamemf<-colnamemf[order(colnamemf$product_rec, colnamemf$product_id, colnamemf$product_name),]
colnames(mat) <- colnamemf$product_name

matriz_rulesmf<-as(mat,"transactions")

```

A continuación se aplica el algoritmo a priori para determinar las reglas de asociación presentes entre los productos del departamento de productos congelados o frozen.

```{r algoritmo_priori_beverages, eval=T, echo=FALSE}
rulesmf<-apriori(matriz_rulesmf,parameter=list(support=0.0002,confidence=0.5))

```


```{r algoritmo_priori_beverages, eval=T, echo=TRUE}

itemFrequencyPlot(matriz_rulesmf, topN=20,  cex.names=.7)
summary(rulesmf)
inspectDT(rulesmf)
plot(rulesmf, measure=c("support","lift"), shading="confidence")
subrules2 <- head(sort(rulesmf, by="lift"), 10)
plot(subrules2, method="graph")
write(rulesmf,file="salidas_rules/rules_frozen.csv",sep=";",dec=",")

```


